import random
import sys


class Ship():
    """
    ship class intializes ship properties from the given line
    """
	
    def __init__(self, line):
        try:
            ship_properties = line.split()
            self.symbol = ship_properties[0]
            self.row_1 = int(ship_properties[1])
            self.col_1 = int(ship_properties[2])
            self.row_2 = int(ship_properties[3])
            self.col_2 = int(ship_properties[4])
            self.length = abs(self.row_2 - self.row_1) + abs(self.col_2 - self.col_1) + 1

        except (ValueError,IndexError):
            print(Game.ERROR_INVALID_FILE)
            sys.exit(0)
     

     
class User():
    """
    User class simulates user actions
    validates ship properties, initializes and diplays user board
    """

    def __init__(self, width, height, filename):
        self.reserved_symbols = ['x', 'o', '*']
        self.symbols = []
        self.width = width
        self.height = height
        self.filename = filename
        self.board = [['*' for i in range(self.width)] for j in range(self.height)]
        self.ships = []
        self.unfired = [(i,j) for i in range(self.height) for j in range(self.width)]
        
    def show_board(self):
        """
        Prints the User board
        """

        print("My Board")
        print('  ' + ' '.join([str(i) for i in range(self.width)]))

        for i in range(self.height):
            print(str(i)+ ' '+ (' '.join(self.board[i])))
        print('')


    def get_board(self):
        """ 
        return the user board
        used for cheating AI
        """
        return self.board

    def place_ship(self,ship):
        """
        Checks whether the symbol is already used, placement is
        overlapped or diagonal and places the ship in the board
        according to the specifications
        """

        #Check for reserved symbol
        if(ship.symbol.lower() in self.reserved_symbols):
            print(Game.ERROR_RESERVED_SYMBOL)
            sys.exit(0)

        #check for duplicate symbol
        if(ship.symbol in self.symbols):
            print(Game.ERROR_DUPLICATE_SYMBOL.format(ship.symbol))
            sys.exit(0)

        #if not used add it to the symbols lists
        self.symbols.append(ship.symbol)


        #check for diagonal placement
        if(ship.row_1 != ship.row_2 and ship.col_1 != ship.col_2):
            print(Game.ERROR_DIAGONAL_PLACEMENT)
            sys.exit(0)

        #check for overflow
        if(ship.row_1 < 0 or ship.row_1 > self.height -1 or
           ship.row_2 < 0 or ship.row_2 > self.height -1 or
           ship.col_1 < 0 or ship.col_1 > self.width - 1 or
           ship.col_2 < 0 or ship.col_2 > self.width - 1):
            
            print(Game.ERROR_PLACEMENT_OVERFLOW.format(ship.symbol))
            sys.exit(0)

        if(ship.row_2 < ship.row_1):
            temp = ship.row_2
            ship.row_2 = ship.row_1
            ship.row_1 = temp

        if(ship.col_2 < ship.col_1):
            temp = ship.col_2
            ship.col_2 = ship.col_1
            ship.col_1 = temp
            
        #check for overlapped placement
        for row in range(ship.row_1 , ship.row_2 + 1):
            for col in range(ship.col_1 , ship.col_2 + 1):
                if(self.board[row][col] == '*'):
                    self.board[row][col] = ship.symbol
                else:
                    print(Game.ERROR_OVERLAPPED_PLACEMENT.format(row,col))
                    sys.exit(0)

        self.ships.append(ship)

    
    def get_ships_from_file(self):
        """
        Reads ship placement file, creates a ship object 
        for each line and places it in the board
        """
        lines = open(self.filename)
        for line in lines:
            line = line.strip()
            s = Ship(line)
            self.place_ship(s)

    def get_ships(self):
        return self.ships
        
    def play(self,ai):
        """
        reads the firing location from user, fires at the location,
        return True if game is over
        """
        prompt = "Enter row and column to fire on separated by a space: "
        fireat = Game.get_input_fireat(prompt, self.width, self.height)

        while(fireat not in self.unfired):
            fireat = Game.get_input_fireat(prompt, self.width, self.height)

        self.unfired.remove(fireat)
        
        ai.fire(fireat[0], fireat[1])
        return ai.game_over()

                
    def fire(self, row, col):
        """
        fires at the location (row, col) in the board,
        updates the value in the board
        checks if any ship sunk
        return True for a hit and False for a miss
        """

        if(self.board[row][col] == '*'):
            print("Miss!")
            self.board[row][col] = 'O'
            return False
        else:
            symbol = self.board[row][col]
            self.board[row][col] = 'X'

            sunk = True
            for row in self.board:
                if symbol in row:
                    sunk = False

            if(sunk):
                print("You sunk my " + symbol)
            else:
                print("Hit!")
    
            return True

    def game_over(self):
        """
        checks for any remaining ships
        return true if all ships are sunk
        """
        for symbol in self.symbols:
            for row in self.board:
                if symbol in row:
                    return False
                    break
            
        return True
    
class AI():
    """
    AI class
    """
    
    def __init__(self, num, width, height):
        self.num = num
        self.ships = []
        self.width = width
        self.height = height
        self.board = [['*' for i in range(self.width)] for j in range(self.height)]
        self.board_hidden = [['*' for i in range(self.width)] for j in range(self.height)]
        self.unfired = [(i,j) for i in range(self.height) for j in range(self.width)]
        self.destroy_mode = False
        self.spots = []
        
    def init_ships(self, ships):
        """
        initialize the list of ships and sorts
        """
        self.ships = sorted(ships, key=lambda ship: ship.symbol)
        


                        
    def place_ships(self):
        """ place the ships randomly"""
        for ship in self.ships:
            placed = False
            #print("random.choice(['vert', 'horz'])")
            while(not placed):
                direction = random.choice(['vert', 'horz'])
                if(direction == 'horz'):

                    placed =  self.place_ship_horz(ship.symbol, ship.length)
                else:

                    placed = self.place_ship_vert(ship.symbol, ship.length)
            
                                
    def place_ship_horz(self, symbol, length):
        """ 
        place the ship horizontally
        
        symbol -- symbol to represent ship
        length -- lenght of ship
        """

        #print('random.randint(0,'+ str(self.height - 1))
        #print('random.randint(0,'+ str(self.width - length))
        row = random.randint(0, self.height - 1)
        col = random.randint(0, self.width - length)
    
        if(''.join(self.board_hidden[row][col:col+length]) == '*' * length):
            print('Placing ship from ' + str(row) + ','+ str(col)+
                  ' to '+ str(row) +','+ str(col + length - 1) + '.')

            for i in range(length):
                self.board_hidden[row][col + i] = symbol
            return True

        return False

    def place_ship_vert(self, symbol, length):
        """ 
        place the ship vertically
        
        symbol -- symbol to represent ship
        length -- lenght of ship
        """

        #print('random.randint(0,'+ str(self.height - length))
        #print('random.randint(0,'+ str(self.width - 1))
        row = random.randint(0, self.height - length)
        col = random.randint(0, self.width - 1)

        for i in range(length):
            if(self.board_hidden[row + i][col] != '*'):
                return False

        print('Placing ship from ' + str(row) + ','+ str(col)+
              ' to '+ str(row + length - 1 ) +','+ str(col) + '.')
        for i in range(length):
            self.board_hidden[row + i][col] = symbol

        return True


    def show_board(self):
        """
        Prints the AI board
        """
        print("Scanning Board")
        print('  ' + ' '.join([str(i) for i in range(self.width)]))
        for i in range(self.height):
            print(str(i)+ ' '+ (' '.join(self.board[i])))
        print('')

    def play(self,user):
        """
        selects the firing position according the operating mode
        update spot list for a hit in smart mode
        """

        if(self.num == 1):
            #random
            fireat = self.get_fireat_random()
        elif(self.num ==2):
            #smarter
            fireat = self.get_fireat_smarter()
        else:
            #cheating
            fireat = self.get_fireat_cheating(user)
            
        print("The AI fires at location ("+ str(fireat[0]) +", "+ str(fireat[1])+")")

        hit = user.fire(fireat[0], fireat[1])
        #print(hit,self.num)
        if(hit and self.num == 2):
            
            #switch to destroy mode
            self.destroy_mode = True
            row,col = fireat[0],fireat[1]
            
            #if not upper most row add above location to spots if its not fired
            if(row != 0 and (row - 1, col) in self.unfired and (row - 1, col) not in self.spots):
                self.spots.append((row - 1, col))
                
            #if not bottom most row add below location to spots if its not fired
            if(row != self.height -1 and (row + 1, col) in self.unfired and (row + 1, col) not in self.spots):
                self.spots.append((row + 1, col))

            #if not left most column add left location to spots if its not fired
            if(col != 0 and (row, col -1) in self.unfired and (row, col -1) not in self.spots):
                self.spots.append((row, col -1))

            #if not right most column add right location to spots if its not fired
            if(col != self.width -1 and (row, col + 1) in self.unfired and (row, col + 1) not in self.spots):
                self.spots.append((row, col + 1))
            #print(self.spots)
        return user.game_over()

    def get_fireat_random(self):
        """randomly select a location from unfired locations"""
        #print('random.choice('+str(self.unfired))
        pos = random.choice(self.unfired)
        self.unfired.remove(pos)
        return pos

    def get_fireat_smarter(self):
        if(not self.destroy_mode): #hunt mode
            return self.get_fireat_random()

        else: #destroy mode
            if(len(self.spots) > 0):
                pos = self.spots.pop(0)
                self.unfired.remove(pos)
                return pos
            else:
                self.destroy_mode = False
                return self.get_fireat_random()
            

    def get_fireat_cheating(self, user):
        """ searches the user board and gets the location of ships"""
        board = user.get_board()
        for row in board:
            for col in row:
                if(col not in ('X', '*')):
                    return (board.index(row), row.index(col))
        
    
    def fire(self, row, col):
        """ 
        fires at the location (row, col), updates the board
        and print message if a ship is sunk
        """
        if(self.board_hidden[row][col] == '*'):
            print("Miss!")
            self.board_hidden[row][col] = 'O'
            self.board[row][col] = 'O'
        else:
            symbol = self.board_hidden[row][col]
            self.board_hidden[row][col] = 'X'
            self.board[row][col] = 'X'

            sunk = True
            for row in self.board_hidden:
                if symbol in row:
                    sunk = False

            if(sunk):
                print("You sunk my " + symbol)
            else:
                print("Hit!")

            

    def game_over(self):
        """
        checks for any remaining ships
        return true if all ships are sunk
        """
        
        for ship in self.ships:
            for row in self.board_hidden:
                if ship.symbol in row:
                    return False
                    break
            
        return True






class Game:
    """
    Game class act as an user interface.
    Controlls the games using the User and AI classes
    """

    #Errors
    ERROR_RESERVED_SYMBOL = "Reserved symbol can't be used"
    ERROR_DUPLICATE_SYMBOL = 'Error symbol {0} is already in use. Terminating game'
    ERROR_INVALID_FILE = "Invalid ship placement file"
    ERROR_DIAGONAL_PLACEMENT = "Ships cannot be placed diagonally. Terminating game."
    ERROR_OVERLAPPED_PLACEMENT = "There is already a ship at location {0}, {1}. Terminating game."
    ERROR_PLACEMENT_OVERFLOW = "Error {0} is placed outside of the board. Terminating game."


    #function to get valid number input form user
    def get_number(prompt, zero = True):
        value = input(prompt);
        
        while(True):
            try:
                value = int(value)
                if(not zero and value <= 0):
                    value = input(prompt)
                else:
                    break
            except ValueError:
                value = input(prompt)
                
        return value

    
    #function to get valid filename input form user	
    def get_filename(prompt):
        value = input(prompt)

        while(True):
            try:
                f = open(value, 'r')
                f.close()
                break
            except FileNotFoundError:
                value = input(prompt)

        return value

    #function to get valid input in a range
    def get_input_range(prompt, Min, Max):
       
        value = Game.get_number(prompt)
        while(True):
            if(value < Min or value > Max):
                value = Game.get_number(prompt)
                pass
            else:
                break

        return value


    def get_input_fireat(prompt, width, height):
        value = input(prompt)
        while(True):
            value = value.split()
            if(len(value) != 2):
                pass

            try:
                row = int(value[0])
                col = int(value[1])
                if(row < 0 or row >= height or col < 0 or col >= width):
                    pass
                return (row, col)
            except(ValueError, IndexError):
                pass

            value = input(prompt)


                                
    """
    run game
    """
    def init():
        seed = Game.get_number("Enter the seed: ")
        width = Game.get_number("Enter the width of the board: ", False)
        height = Game.get_number("Enter the height of the board: ", False)
        filename = Game.get_filename("Enter the name of the file containing your ship placements: ")
        ai_number = Game.get_input_range("Choose your AI.\n 1. Random\n 2. Smart\n 3. Cheater\n Your choice: ", 1, 3)

        random.seed(seed)#seed the random generator
        
        user = User(width, height, filename)
        user.get_ships_from_file()

        ai = AI(ai_number, width, height)
        ai.init_ships(user.get_ships())

        ai.place_ships()

        #print('random.randint(0,1)')
        start = random.randint(0,1)
        if(start):
            game_over = ai.play(user)
            if(game_over):
                Game.game_over(ai,user,"The AI wins.")
                sys.exit(0)
            
        while(True):
            ai.show_board()               
            user.show_board()
        
            game_over = user.play(ai)
            if(game_over):
                Game.game_over(ai,user,"You win!")
                break
               
            game_over = ai.play(user)
            if(game_over):
                Game.game_over(ai,user,"The AI wins.")
                break

               


    def game_over(a, b, message):
        a.show_board()
        b.show_board()
        print(message)

                        
if(__name__ == '__main__'):
    Game.init()
