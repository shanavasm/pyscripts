#get dictionary file name
dict_file_name = input()

#get the letters
word = input().replace(" ", '') # remove spaces from the input

word_length = len(word) + 1
dict_words = []
dict_file = open(dict_file_name);

"""
@word_length to store the number of letters given
@dict_words list that stores all the words
@dict_file dictionary file resource

initialize @dict_words with the words from dictionary file
"""

for line in dict_file:
    if(len(line) <= word_length):
        dict_words.append(line.strip())


wordlist = []


"""
permutations function:
calculates all the possible combination of letters recursively
and for each substring check whether the word is in dictionary
and add to the wordlist
"""

def permutations(word):
    if len(word)<=1:
        return [word]

    perms = permutations(word[1:])
    char = word[0]
    result = []
    for perm in perms:
        for i in range(len(perm) + 1):
            newword = perm[:i] + char + perm[i:]
            result.append(newword)
            for j in range(len(newword)):
                another = newword[j:]
                if(dict_words.count(another) != 0 and wordlist.count(another) == 0):
                    dict_words.remove(another)
                    wordlist.append(another)

    return result

permutations(word)
wordlist.sort()

for item in wordlist:
    print(item)
