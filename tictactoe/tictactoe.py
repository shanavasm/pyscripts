#!/usr/bin/python

##################################
#
# tic-tac-toe game
#
##################################

#global variables

#initialize board
board = [[' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' ']]
# game status
game_over = False

# player name
player = 'Player1';
# player symbol
symbol = 'X'
winner = None
def main():
    global board, game_over, player
    
    while( not game_over ):
        print(player + "'s turn")
        display_board();
        
        while( True ):
            row_col = get_input()
            row = row_col[0]
            col = row_col[1]

            if( board[row][col] == ' ' ):
                break
            else:
                print('This column is already marked')

        #update the board
        board[row][col] = symbol

        check_game_over();
        toggle_player()


    display_board()
    print("Game over")
    if(winner):
        print(winner + ' wins')
    else:
        print('Game tied up')

# function to display the board
def display_board():
    global board
    print('+---' * 3 + '+')
    
    for row in board:
        print('| ' + ' | '.join(row) + ' |')
        print('+---' * 3 + '+')
    

#function to get user input        
def get_input():
    row = get_valid_input('row')
    col = get_valid_input('col')
    return [row - 1, col - 1 ]            #convert to index numbers


#validates the input
def get_valid_input(var):
    value = None
    
    while( not value or value < 1 or value > 3):
        try:
            value  = int( input("Enter " + var + ': ') )
        except:
            pass

    return value;
    

# toggle the player and symbol    
def toggle_player():
    global player, symbol
    
    if( player == 'Player1' ):
        player = 'Player2'
        symbol = 'O'
    else:
        player = 'Player1'        
        symbol = 'X'

def check_game_over():
    global game_over,board,symbol, player, winner

    #row check
    for row in board:
        if (''.join(row) == symbol * 3):
            game_over = True
            winner = player
            return

    #column check
    for col in range(0, 2):
        if( board[0][col] == board[1][col] == board[2][col] == symbol):
            game_over = True
            winner = player
            return

    #diagonal check
    if(board[1][1] == symbol and ( (board[0][0] == board[2][2] == symbol) or board[0][2] == board[2][0] == symbol)):
        game_over = True
        winner = player
        return

    #check for tie
    for row in board:
        if( row.count(' ') ):
            return
    game_over = True
        
if __name__ == '__main__':
    main()    
